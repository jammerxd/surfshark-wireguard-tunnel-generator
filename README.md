# surfshark-wireguard-tunnel-generator



## Getting started

Loosely based on the work done by PolicyPuma4 at https://github.com/PolicyPuma4/surfshark-wireguard-tunnel-generator

I added the ability to set a save directory in the code and the ability to use custom DNS servers since I use pi-hole/adguard-home

Pro-tip: if you have multiple devices - use a file sharing service like dropbox or google drive or amazon s3 to distribute the config files generated to all your devices
