﻿using System.Net;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace SurfsharkWireguardTunnelGenerator
{
    class Server
    {
        [JsonPropertyName("connectionName")]
        public string ConnectionName { get; set; }
        [JsonPropertyName("pubKey")]
        public string PubKey { get; set; }
    }
    class Settings
    {
        public string SaveDirectory { get; set; }
        public string DNSServers { get; set; }
    }
    internal class Program
    {
        static async Task Main(string[] args)
        {

            Settings settings = new Settings()
            {
                SaveDirectory = Path.Combine(Directory.GetCurrentDirectory(),"surfshark-wireguard-tunnel-generator"),
                DNSServers = ""

            };
            if(File.Exists("settings.json"))
            {
                var fileSettings = JsonSerializer.Deserialize<Settings>(File.ReadAllText("settings.json"));
                if(!string.IsNullOrEmpty(fileSettings.SaveDirectory))
                {
                    settings.SaveDirectory = fileSettings.SaveDirectory;
                }
                settings.DNSServers = fileSettings.DNSServers;
                Console.WriteLine(string.Format("Using {0} for save directory", settings.SaveDirectory));
                Console.WriteLine(string.Format("Using {0} for dns servers ", settings.DNSServers));
            }
            else
            {
                Console.WriteLine("Enter the save directory to save the config files in(press ENTER for default): ");
                var saveDir = Console.ReadLine().Trim();
                if(!String.IsNullOrEmpty(saveDir))
                {
                    settings.SaveDirectory = saveDir;
                }
                Console.WriteLine("Enter the DNS servers you wish to use as a comma-separated list(press ENTER for default):");
                var dnsServers = Console.ReadLine().Trim();
                if(!string.IsNullOrEmpty(dnsServers))
                {
                    settings.DNSServers = dnsServers;
                }
                File.WriteAllText("settings.json", JsonSerializer.Serialize(settings));
            }
            Console.WriteLine();
            Console.WriteLine("Connect to the surfshark vpn with wireguard protocol");
            Console.WriteLine("Press ENTER to continue");
            Console.ReadLine();

            string[] lines = File.ReadAllLines(Path.Combine(Environment.GetEnvironmentVariable("PROGRAMDATA"),"Surfshark","WireguardConfigs","SurfsharkWireGuard.conf"));
            for(int i = 0; i < lines.Length; i++)
            {
                if(lines[i].StartsWith("DNS = ") && !String.IsNullOrEmpty(settings.DNSServers))
                {
                    lines[i] = "DNS = {{DNS}}";
                    continue;
                }
                if(lines[i].StartsWith("PublicKey = "))
                {
                    lines[i] = "PublicKey = {{KEY}}";
                    continue;
                }
                
                if(lines[i].StartsWith("Endpoint = "))
                {
                    var uri = lines[i].Trim().Substring(lines[i].IndexOf("=") + 1).Trim();
                    var port = uri.Substring(uri.LastIndexOf(":")+1);
                    lines[i] = "Endpoint = {{ENDPOINT}}:" + port;
                    break;
                }
            }
            StringBuilder sb = new StringBuilder();
            foreach(var line in lines) sb.AppendLine(line);

            string template = sb.ToString();

            List<Server> servers = new List<Server>();

            string[] endpointTypes = new string[] { "generic", "double", "static", "obfuscated" };

            using (var client = new HttpClient())
            {
                foreach (var endpointType in endpointTypes)
                {
                    var url = string.Format("https://api.surfshark.com/v4/server/clusters/{0}?countryCode=", endpointType);
                    var response = await client.GetFromJsonAsync<List<Server>>(url);
                    if(response != null) servers.AddRange(response);
                }
            }

            int count = 0;
            if(servers.Count > 0)
            {
                if(!Directory.Exists(settings.SaveDirectory))
                {
                    Directory.CreateDirectory(settings.SaveDirectory);
                }
                if(!Directory.Exists(Path.Combine(settings.SaveDirectory,"android")))
                {
                    Directory.CreateDirectory(Path.Combine(settings.SaveDirectory, "android"));
                }
            }
            foreach(var server in servers)
            {
                if (string.IsNullOrEmpty(server.PubKey)) continue;
                string connectionTemplate = template;
                connectionTemplate = connectionTemplate.Replace("{{ENDPOINT}}", server.ConnectionName);
                connectionTemplate = connectionTemplate.Replace("{{KEY}}", server.PubKey);
                connectionTemplate = connectionTemplate.Replace("{{DNS}}", settings.DNSServers);
                File.WriteAllText(Path.Combine(settings.SaveDirectory,String.Format("{0}.conf",server.ConnectionName)), connectionTemplate);
                if(String.Compare(server.ConnectionName, "us-den.prod.surfshark.com") == 0)
                {
                    File.WriteAllText(Path.Combine(settings.SaveDirectory,"android","denver.conf"), connectionTemplate);
                    count++;
                }
                if (String.Compare(server.ConnectionName, "us-chi.prod.surfshark.com") == 0)
                {
                    File.WriteAllText(Path.Combine(settings.SaveDirectory, "android", "chicago.conf"), connectionTemplate);
                    count++;
                }
                count++;

            }
            Console.WriteLine(String.Format("Wrote {0} files", count.ToString()));
            Console.WriteLine("Press ENTER to exit");
            Console.ReadLine();

        }
    }
}